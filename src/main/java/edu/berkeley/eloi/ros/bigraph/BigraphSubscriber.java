package edu.berkeley.eloi.ros.bigraph;

import edu.berkeley.eloi.bigvis.Visualizer;
import org.apache.commons.logging.Log;
import org.ros.message.MessageListener;
import org.ros.namespace.GraphName;
import org.ros.node.AbstractNodeMain;
import org.ros.node.ConnectedNode;
import org.ros.node.topic.Subscriber;
import std_msgs.*;
import std_msgs.String;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * User: eloi
 * Date: 5/28/13
 * Time: 8:26 AM
 * To change this template use File | Settings | File Templates.
 */
public class BigraphSubscriber extends AbstractNodeMain {

    Visualizer frame = new Visualizer("");

    @Override
    public GraphName getDefaultNodeName() {
        return GraphName.of("ros_bigraph_visualizer/bigraph_sub");
    }

    @Override
    public void onStart(ConnectedNode connectedNode) {
        frame.setVisible(true);
        final Component comp = frame.getComponent(0);
        final Log log = connectedNode.getLog();
        Subscriber<std_msgs.String> subscriber = connectedNode.newSubscriber("bgm", std_msgs.String._TYPE);
        subscriber.addMessageListener(new MessageListener<String>() {
            @Override
            public void onNewMessage(std_msgs.String message) {
                log.info("I heard: \"" + message.getData() + "\"");
                frame.update(message.getData());
                BufferedImage image = new BufferedImage(frame.getWidth(), frame.getHeight(), BufferedImage.TYPE_INT_RGB);
                Graphics2D g2 = image.createGraphics();
                g2.setPaint(Color.white);
                g2.fillRect(0,0,frame.getWidth(),frame.getHeight());
                comp.paint(g2);
                g2.dispose();
                try {
                    ImageIO.write(image, "png", new File("example.png"));
                } catch (IOException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        });
    }
}

