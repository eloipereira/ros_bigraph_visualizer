package edu.berkeley.eloi.ros.bigraph;

import big_actor_msgs.Vehicle;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geom.impl.CoordinateArraySequence;
import edu.berkeley.eloi.bigraph.Bigraph;
import edu.berkeley.eloi.bigraph.Node;
import edu.berkeley.eloi.bigraph.Place;
import edu.berkeley.eloi.bigraph.PlaceList;
import org.apache.commons.logging.Log;
import org.ros.concurrent.CancellableLoop;
import org.ros.message.MessageListener;
import org.ros.namespace.GraphName;
import org.ros.node.AbstractNodeMain;
import org.ros.node.ConnectedNode;
import org.ros.node.topic.Publisher;
import org.ros.node.topic.Subscriber;

import edu.berkeley.eloi.bigraph.*;
import std_msgs.String;
import big_actor_msgs.StructureStateEstimate;
import big_actor_msgs.*;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.geom.impl.CoordinateArraySequence;



import java.util.ArrayList;
import java.util.List;

/**
 * Node publishing a bigraph as a bgm term.
 *
 * @author eloi@berkeley.edu (Eloi Pereira)
 */
public class SSE2Bigraph extends AbstractNodeMain {

    private java.lang.String term = "";
    private List<java.lang.String> signature = new ArrayList<java.lang.String>();
    private List<java.lang.String> names = new ArrayList<java.lang.String>();


    @Override
    public GraphName getDefaultNodeName() {
        return GraphName.of("ros_bigraph_visualizer/SSE2Bigraph");
    }

    @Override
    public void onStart(final ConnectedNode connectedNode) {

        final Log log = connectedNode.getLog();

        final Publisher<std_msgs.String> publisher =
                connectedNode.newPublisher("bgm", std_msgs.String._TYPE);
        // This CancellableLoop will be canceled automatically when the node shuts
        // down.

        final Publisher<ros_bigraph_visualizer.Bigraph> publisher0 =
                connectedNode.newPublisher("bigraph", ros_bigraph_visualizer.Bigraph._TYPE);

        final ros_bigraph_visualizer.Bigraph bg = connectedNode.getTopicMessageFactory().newFromType(ros_bigraph_visualizer.Bigraph._TYPE);

        Subscriber<big_actor_msgs.StructureStateEstimate> subscriber = connectedNode.newSubscriber("sse", StructureStateEstimate._TYPE);
        subscriber.addMessageListener(new MessageListener<StructureStateEstimate>() {
            @Override
            public void onNewMessage(StructureStateEstimate message) {
                log.info("SSE received \n");
                log.info("Source vehicle: " + message.getSrcVehicleId());
                term = sse2bigraph(message).getTerm();
                log.info("Bigraph term: "+ term);
                signature = sse2signature(message);
                names = sse2names(message);


                bg.setBgm(term);
                bg.setNames(names);
                bg.setSignature(signature);
                log.info("Bigraph:" + bg);
            }
        });

        connectedNode.executeCancellableLoop(new CancellableLoop() {


            @Override
            protected void setup() {
                bg.setBgm(term);
                bg.setNames(names);
                bg.setSignature(signature);
            }

            @Override
            protected void loop() throws InterruptedException {
                std_msgs.String str = publisher.newMessage();
                str.setData(term);
                publisher.publish(str);
                publisher0.publish(bg);
                Thread.sleep(5000);
            }
        });
    }

    public static Bigraph sse2bigraph(StructureStateEstimate sse){
        PlaceList places = new PlaceList();
        Region rg0 = new Region(0);
        places.add(rg0);

        for (Location l : sse.getLocations()){
            Place prt = findLocationParent(l,sse,rg0);
            //System.out.println("Parent of " + l.getName() + ": "+ prt);
            places.add(new Node(l.getName(),"Location",new ArrayList<java.lang.String>(),prt));

        }
        for (Vehicle v : sse.getVehicles()){
            Place prt = findVehicleParent(v, sse, rg0);
            List<Network> networks = v.getNetworks();
            List<java.lang.String> names0 = new ArrayList<java.lang.String>();
            for (Network n: networks){
                if (n.getType() == Network.NT_ETHERNET_10M
                        || n.getType() == Network.NT_ETHERNET_100M
                        || n.getType() == Network.NT_ETHERNET_1G
                        || n.getType() == Network.NT_ETHERNET_10G
                        && ! names0.contains("ethernet")) names0.add("ethernet");
                if (n.getType() == Network.NT_WIFI_2MB
                        || n.getType() == Network.NT_WIFI_11MB
                        || n.getType() == Network.NT_WIFI_27MB
                        || n.getType() == Network.NT_WIFI_54MB
                        || n.getType() == Network.NT_WIFI_600MB
                        || n.getType() == Network.NT_WIFI_1300MB
                        || n.getType() == Network.NT_WIFI_7000MB
                        && ! names0.contains("wifi")) names0.add("wifi");
                if (n.getType() == Network.NT_MESH_500kB && ! names0.contains("mesh")) names0.add("mesh");
                if (n.getType() == Network.NT_ACOUSTIC_7kB
                        || n.getType() == Network.NT_ACOUSTIC_9kB
                        || n.getType() == Network.NT_ACOUSTIC_14kB
                        || n.getType() == Network.NT_ACOUSTIC_31kB
                        && ! names0.contains("acoustic")) names0.add("acoustic");
                if (n.getType() == Network.NT_PICCOLO && ! names0.contains("piccolo")) names0.add("piccolo");
                if (n.getType() == Network.NT_AIS && ! names0.contains("ais")) names0.add("ais");
            }
            places.add(new Node(v.getName(),"Vehicle", names0, prt));
        }

        Bigraph bg = new Bigraph(places);
        //System.out.println(bg);
        return bg;
    }

    private static List<java.lang.String> sse2signature(StructureStateEstimate sse){
        List<java.lang.String> signature = new ArrayList<java.lang.String>();

        for(Location l: sse.getLocations()){
            if (l.getLocationType() == Location.LT_NONE && ! signature.contains("none")) signature.add("none");
            if (l.getLocationType() == Location.LT_AIR_SPACE && ! signature.contains("air_space")) signature.add("air_space");
            if (l.getLocationType() == Location.LT_OIL_SPILL && ! signature.contains("oil_spill")) signature.add("oil_spill");
            if (l.getLocationType() == Location.LT_SURFACE && ! signature.contains("surface")) signature.add("surface");
            if (l.getLocationType() == Location.LT_UNDERWATER && ! signature.contains("underwater")) signature.add("underwater");
        }

        for(Vehicle v: sse.getVehicles()){
            if (v.getVehicleType() == Vehicle.VT_DRIFTER && ! signature.contains("drifter")) signature.add("drifter");
            if (v.getVehicleType() == Vehicle.VT_GROUND_STATION && ! signature.contains("ground_station")) signature.add("ground_station");
            if (v.getVehicleType() == Vehicle.VT_MODEL_AIRPLANE && ! signature.contains("model_airplane")) signature.add("model_airplane");
            if (v.getVehicleType() == Vehicle.VT_NONE && ! signature.contains("none")) signature.add("none");
            if (v.getVehicleType() == Vehicle.VT_QUADROTOR && ! signature.contains("quadrotor")) signature.add("quadrotor");
            if (v.getVehicleType() == Vehicle.VT_VESSEL && ! signature.contains("vessel")) signature.add("vessel");
        }
        return signature;
    }

    private static List<java.lang.String> sse2names(StructureStateEstimate sse){
        List<java.lang.String> names = new ArrayList<java.lang.String>();
        for (Network n: sse.getNetworks()){
            if (n.getType() == Network.NT_ETHERNET_10M
                    || n.getType() == Network.NT_ETHERNET_100M
                    || n.getType() == Network.NT_ETHERNET_1G
                    || n.getType() == Network.NT_ETHERNET_10G
                    && ! names.contains("ethernet")) names.add("ethernet");
            if (n.getType() == Network.NT_WIFI_2MB
                    || n.getType() == Network.NT_WIFI_11MB
                    || n.getType() == Network.NT_WIFI_27MB
                    || n.getType() == Network.NT_WIFI_54MB
                    || n.getType() == Network.NT_WIFI_600MB
                    || n.getType() == Network.NT_WIFI_1300MB
                    || n.getType() == Network.NT_WIFI_7000MB
                    && ! names.contains("wifi")) names.add("wifi");
            if (n.getType() == Network.NT_MESH_500kB && ! names.contains("mesh")) names.add("mesh");
            if (n.getType() == Network.NT_ACOUSTIC_7kB
                    || n.getType() == Network.NT_ACOUSTIC_9kB
                    || n.getType() == Network.NT_ACOUSTIC_14kB
                    || n.getType() == Network.NT_ACOUSTIC_31kB
                    && ! names.contains("acoustic")) names.add("acoustic");
            if (n.getType() == Network.NT_PICCOLO && ! names.contains("piccolo")) names.add("piccolo");
            if (n.getType() == Network.NT_AIS && ! names.contains("ais")) names.add("ais");
        }
        return names;
    }

    private static Place findLocationParent(Location loc, StructureStateEstimate sse, Region region){
        Location container = findMinimumContainer(loc,sse.getLocations());
        if (container.equals(loc)){
            return region;
        } else {
            return new Node(container.getName(),"Location",new ArrayList<java.lang.String>(),region);
        }

    }

    private static Place findVehicleParent(Vehicle v, StructureStateEstimate sse, Region region){
        Place parent = null;

        Coordinate vehCoord = new Coordinate(v.getPosition().getLatitude(), v.getPosition().getLongitude());
        GeometryFactory gf = new GeometryFactory();
        Point vehPoint = gf.createPoint(vehCoord);

        for(Location l : sse.getLocations()){
            List<LatLng> ll = l.getBoundaries();
            List<Coordinate> points = new ArrayList<Coordinate>();
            for(LatLng point : ll){
                points.add(new Coordinate(point.getLatitude(), point.getLongitude()));
            }

            Polygon polygon = gf.createPolygon(new LinearRing(new CoordinateArraySequence(points
                    .toArray(new Coordinate[points.size()])), gf), null);

            if(vehPoint.within(polygon)){
                parent = new Node(l.getName(),"Location",new ArrayList<java.lang.String>(),region);
                //System.out.println("Found parent of " + v + ": " + parent);
            }
        }

        if (parent == null){
            parent = region;
        }
        return parent;
    }

    private static Location findMinimumContainer(Location loc, List<Location> locations){
        Location minContainer  = loc;

        GeometryFactory gf = new GeometryFactory();

        List<LatLng> locVertices = loc.getBoundaries();
        List<Coordinate> locCoord = new ArrayList<Coordinate>();
        for(LatLng point : locVertices){
            locCoord.add(new Coordinate(point.getLatitude(), point.getLongitude()));
        }

        Polygon locPolygon = gf.createPolygon(new LinearRing(new CoordinateArraySequence(locCoord
                .toArray(new Coordinate[locCoord.size()])), gf), null);
        locPolygon = (Polygon) locPolygon.convexHull();

        Double minContainerArea = Double.MAX_VALUE;

        Double maxIntersectionArea = 0.0;
        Boolean foundContainer = false;

        for(Location l_t : locations){
            List<LatLng> ll = l_t.getBoundaries();
            List<Coordinate> points = new ArrayList<Coordinate>();
            for(LatLng point : ll){
                points.add(new Coordinate(point.getLatitude(), point.getLongitude()));
            }

            Polygon polygon = gf.createPolygon(new LinearRing(new CoordinateArraySequence(points
                    .toArray(new Coordinate[points.size()])), gf), null);

            polygon = (Polygon) polygon.convexHull();


            if(polygon.contains(locPolygon) && polygon.getArea() < minContainerArea && polygon.getArea() > locPolygon.getArea()){
                minContainer=l_t;
                minContainerArea = polygon.getArea();
                foundContainer = true;
            } else if(!foundContainer && locPolygon.overlaps(polygon))
                if(locPolygon.intersection(polygon).getArea() > maxIntersectionArea) {
                    maxIntersectionArea = locPolygon.intersection(polygon).getArea();
                    if(maxIntersectionArea > 0.5 * locPolygon.getArea()){
                        minContainer=l_t;
                        //System.out.println("Found intersection of " + loc.getName() + " with " + l_t.getName());
                    }
                }

        }
        //System.out.println("Minimum container of "+ loc.getName() + ": "+ minContainer.getName());
        return minContainer;
    }



}