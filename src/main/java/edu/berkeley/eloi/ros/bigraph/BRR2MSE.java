package edu.berkeley.eloi.ros.bigraph;

import big_actor_msgs.*;
import big_actor_msgs.Location;
import com.vividsolutions.jts.geom.*;
import com.vividsolutions.jts.geom.impl.CoordinateArraySequence;
import edu.berkeley.eloi.bigraph.*;
import org.antlr.runtime.RecognitionException;
import org.apache.commons.logging.Log;
import org.ros.concurrent.CancellableLoop;
import org.ros.internal.message.RawMessage;
import org.ros.message.MessageListener;
import org.ros.namespace.GraphName;
import org.ros.node.AbstractNodeMain;
import org.ros.node.ConnectedNode;
import org.ros.node.topic.Publisher;
import org.ros.node.topic.Subscriber;
import std_msgs.String;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Node publishing a bigraph as a bgm term.
 *
 * @author eloi@berkeley.edu (Eloi Pereira)
 */
public class BRR2MSE extends AbstractNodeMain {
    private Bigraph bigraph = new Bigraph();


    @Override
    public GraphName getDefaultNodeName() {
        return GraphName.of("ros_bigraph_visualizer/BRR2MSE");
    }

    @Override
    public void onStart(final ConnectedNode connectedNode) {

        final Log log = connectedNode.getLog();

        final StructureStateEstimate sse = connectedNode.getTopicMessageFactory().newFromType(StructureStateEstimate._TYPE);
        final MissionStateEstimate mse = connectedNode.getTopicMessageFactory().newFromType(MissionStateEstimate._TYPE);


        final Publisher<MissionStateEstimate> publisher =
                connectedNode.newPublisher("mse", MissionStateEstimate._TYPE);

        Subscriber<big_actor_msgs.StructureStateEstimate> subscriber0 = connectedNode.newSubscriber("sse", StructureStateEstimate._TYPE);
        subscriber0.addMessageListener(new MessageListener<StructureStateEstimate>() {
            @Override
            public void onNewMessage(StructureStateEstimate message) {
                log.info("SSE received \n");
                sse.setConnections(message.getConnections());
                sse.setLocations(message.getLocations());
                sse.setNetworks(message.getNetworks());
                sse.setSrcVehicleId(message.getSrcVehicleId());
                sse.setTimeStamp(message.getTimeStamp());
                sse.setVehicles(message.getVehicles());
                java.lang.String bigraphTerm = SSE2Bigraph.sse2bigraph(message).getTerm();

                try {
                    bigraph = new Bigraph(bigraphTerm+";");
                } catch (IOException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                } catch (RecognitionException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }

            }
        });

        Subscriber<big_actor_msgs.MissionStateEstimate> subscriber1 = connectedNode.newSubscriber("mse", MissionStateEstimate._TYPE);
        subscriber1.addMessageListener(new MessageListener<MissionStateEstimate>() {
            @Override
            public void onNewMessage(MissionStateEstimate message) {
                log.info("MSE received \n");
                mse.setSrcVehicleId(message.getSrcVehicleId());
                mse.setTasks(message.getTasks());
                mse.setTimeStamp(message.getTimeStamp());
                log.info(message.getTasks());
            }
        });

        Subscriber<String> subscriber2 = connectedNode.newSubscriber("brr", String._TYPE);
        subscriber2.addMessageListener(new MessageListener<String>() {
            @Override
            public void onNewMessage(String message) {
                log.info("BRR received \n");
                if (sse == null) log.info("Structure unknown. BRR ignored.");
                else {
                    Task t = null;

                    try {
                        BRR brr = null;
                        try {
                            log.info(message.getData());
                            brr = new BRR(message.getData());
                        } catch (RecognitionException e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
                        Bigraph redex = brr.getRedex();
                        Bigraph reactum = brr.getReactum();

                        List<Node> vehicles = redex.getNodesWithControlID("Vehicle");
                        if(vehicles.size() == 0) log.info("No vehicles to move");
                        else if (vehicles.size() > 1) log.info("Rule attempt to move more than one vehicle.");
                        else {
                            Node vehicleNode = vehicles.get(0);
                            Vehicle vehicle = getVehicleWithName(sse,vehicleNode.getId());
                            if(vehicle == null) log.info("Vehicle does not exist or is not declared in the current structure estimation");
                            else if(reactum.getNode(vehicleNode.getId()) == null) log.info("Vehicle not included in the reactum.");
                            else {
                                if(!reactum.getNode(vehicleNode.getId()).getParent().getClass().equals(Node.class)) log.info("Destination is not a node.");
                                else {
                                    Node prnt = (Node) reactum.getNode(vehicleNode.getId()).getParent();
                                    if(prnt.getCtrId().equals("Location")) {
                                        Location location = getLocationWithName(sse,prnt.getId());
                                        if (location == null) log.info("Destination does not exist or is not declared in the current structure estimation");
                                        else {
                                            Point centroid = getLocationCentroid(location);
                                            log.info("Create waypoint for vehicle " + vehicle.getName() + " at "+ centroid.getX() + ", " + centroid.getY());
                                            t = createWPTask(vehicle,centroid.getX(),centroid.getY(),vehicle.getPosition().getAltitude());

                                        }
                                    } else if (prnt.getCtrId().equals("Vehicle")) {
                                        Vehicle destVehicle = getVehicleWithName(sse, prnt.getId());
                                        if (destVehicle == null) log.info("Destination does not exist or is not declared in the current structure estimation");
                                        else {
                                            log.info("Create waypoint for vehicle " + vehicle.getName() + " at "+ destVehicle.getPosition().getLatitude() + ", " + destVehicle.getPosition().getLongitude());
                                            t = createWPTask(vehicle,destVehicle.getPosition().getLatitude(),destVehicle.getPosition().getLongitude(),vehicle.getPosition().getAltitude());
                                        }
                                    }


                                    if(t != null){
                                        List<Task> tasks = mse.getTasks();
                                        tasks.add(t);

                                        //set new MSE fields
                                        mse.setTimeStamp(System.currentTimeMillis());
                                        mse.setSrcVehicleId(0);
                                        mse.setTasks(tasks);
                                        publisher.publish(mse);
                                    }
                                }
                            }
                        }
                    } catch (IOException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
            }
        });
    }

    private Task createWPTask(Vehicle v, Double lat, Double lon, Double alt){
        Task t = null;
        Long time = System.currentTimeMillis();
        t.setTaskStamp(time);
        t.setTaskId(time.intValue());
        t.setTaskType(Task.TT_GOTO_WP);
        t.setStatus(Task.TS_ASSIGNED);
        t.setCreationStamp(time);
        t.setVehicleId(v.getVehicleId());
        java.lang.String jsonParam = "{\n" +
                "\t \"latitude\": " + lat + "\"" +
                "\t \"longitude\": " + lon + "\"" +
                "\t \"altitude\": " + alt + "\"" +
                "}";

        t.setParameters(jsonParam);
        return t;
    }

    private Vehicle getVehicleWithName(StructureStateEstimate sse, java.lang.String name){
        Vehicle vehicle = null;
        for(Vehicle v: sse.getVehicles()){
            if(v.getName().equals(name)) vehicle = v;
        }
        return vehicle;
    }

    private Location getLocationWithName(StructureStateEstimate sse, java.lang.String name){
        Location loc = null;
        for(Location l: sse.getLocations()){
            if(l.getName().equals(name)) loc = l;
        }
        return loc;
    }

    private Point getLocationCentroid(Location loc){

        GeometryFactory gf = new GeometryFactory();
        List<LatLng> locVertices = loc.getBoundaries();
        List<Coordinate> locCoord = new ArrayList<Coordinate>();
        for(LatLng point : locVertices){
            locCoord.add(new Coordinate(point.getLatitude(), point.getLongitude()));
        }

        Polygon locPolygon = gf.createPolygon(new LinearRing(new CoordinateArraySequence(locCoord
                .toArray(new Coordinate[locCoord.size()])), gf), null);
        locPolygon = (Polygon) locPolygon.convexHull();

        return locPolygon.getCentroid();

    }
}